require('dotenv').config();
const { UserGame } = require('../models');
const request = require('supertest');
const app     = require('../app');
const jwt     = require('jsonwebtoken');
const {
  JWT_SECRET_KEY,
  JWT_EXPIRE_TIME
} = process.env;

const token = jwt.sign({
  id: "1403603b-1a25-487d-b33a-88c6ffc77b37",
  username: "binar_std1"
}, JWT_SECRET_KEY, { expiresIn: JWT_EXPIRE_TIME })

describe("POST add user", () => {
  it("Successfully added user", done => {
    request(app)
      .post("/users")
      .set("Accept", "application/json")
      .set("X-Api-Token", `Bearer ${token}`)
      .send({
        username: "binar_std2",
        password: "binar123",
        name: "student binar"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(201);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("data successfully added");
          done()
        }
      });
  });

  it("Error Unprocessed Entity", done => {
    request(app)
      .post("/users")
      .set("Accept", "application/json")
      .set("X-Api-Token", `Bearer ${token}`)
      .send({
        username: "binar_",
        password: "binar123",
        name: "student binar"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(422);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Validation error: Validation len on username failed");
          done()
        }
      });
  });

  it("Error No Auth", done => {
    request(app)
      .post("/users")
      .set("Accept", "application/json")
      .send({
        username: "binar_std2",
        password: "binar123",
        name: "student binar"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done()
        }
      });
  });

  it("Error Wrong token", done => {
    request(app)
      .post("/users")
      .set("Accept", "application/json")
      .set("X-Api-Token", "ah9sdha9sd98hasd")
      .send({
        username: "binar_std2",
        password: "binar123",
        name: "student binar"
      })
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      });
  });
})

describe("GET list users", () => {
  it("Success", done => {
    request(app)
      .get('/users')
      .set("X-Api-Token", `Bearer ${token}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(200);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("data successfully retrieved");
          expect(res.body).not.toBeNull();
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .get('/users')
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          expect(res.body).not.toBeNull();
          done();
        }
      })
  });

  it("Error wrong token", done => {
    request(app)
      .get('/users')
      .set("X-Api-Token", `asdasdasd`)
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err)
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });
});

describe("GET detail user", () => {
  it("Successfully get details data", done => {
    request(app)
      .get(`/users/1403603b-1a25-487d-b33a-88c6ffc77b37`)
      .set("Accept", "application/json")
      .set("X-Api-Token", `Bearer ${token}`)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("data successfully retrieved");
          done();
        }
      })
  });

  it("Error data not found", done => {
    request(app)
      .get(`/users/1403603b-1a25-487d-b33a-88c6ffc77b3`)
      .set("Accept", "application/json")
      .set("X-Api-Token", `Bearer ${token}`)
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("data not found");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .get("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });

  it("Error wrong token", done => {
    request(app)
      .get("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("X-Api-Token", "ahsdiahsd9")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });
});

describe("POST add history game", () => {
  it("Successfully add history game", done => {
    request(app)
      .post("/users/1403603b-1a25-487d-b33a-88c6ffc77b37/history")
      .set("Accept", "application/json")
      .set("X-Api-Token", `Bearer ${token}`)
      .send({
        point: 10,
        level: 15
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(201);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("game history successfully added");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .post("/users/1403603b-1a25-487d-b33a-88c6ffc77b37/history")
      .set("Accept", "application/json")
      .send({
        point: 10,
        level: 15
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .post("/users/1403603b-1a25-487d-b33a-88c6ffc77b37/history")
      .set("X-Api-Token", "auhsda9sda")
      .set("Accept", "application/json")
      .send({
        point: 10,
        level: 15
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });
});

describe("PUT user game", () => {
  it("Successfully update user game", done => {
    request(app)
      .put("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("X-Api-Token", `Bearer ${token}`)
      .set("Accept", "application/json")
      .send({
        name: "Senkuu",
        age: 21,
        about_me: "lorem ipsum"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("data successfully updated");
          done();
        }
      })
  });

  it("Error Not Found", done => {
    request(app)
      .put("/users/1403603b-1a25-487d-b33a-88c6ffc77b3")
      .set("X-Api-Token", `Bearer ${token}`)
      .set("Accept", "application/json")
      .send({
        name: "Senkuu",
        age: 21,
        about_me: "lorem ipsum"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("data not found");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .put("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("Accept", "application/json")
      .send({
        name: "Senkuu",
        age: 21,
        about_me: "lorem ipsum"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .put("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("X-Api-Token", "auhsda9sda")
      .set("Accept", "application/json")
      .send({
        name: "Senkuu",
        age: 21,
        about_me: "lorem ipsum"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });
});

describe("DELETE user game", () => {
  it("Error Not Found", done => {
    request(app)
      .delete("/users/1403603b-1a25-487d-b33a-88c6ffc77b3")
      .set("X-Api-Token", `Bearer ${token}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("data not found");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .delete("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });

  it("Error No Auth", done => {
    request(app)
      .delete("/users/1403603b-1a25-487d-b33a-88c6ffc77b37")
      .set("X-Api-Token", "auhsda9sda")
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("Invalid Token");
          done();
        }
      })
  });

  it("Successfully deleted user game", done => {
    request(app)
      .delete("/users/1403603b-1a25-487d-b33a-88c6ffc77b3")
      .set("X-Api-Token", `Bearer ${token}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(404);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("data not found");
          done();
        }
      })
  });
})