const request = require('supertest');
const app     = require('../app');

describe("POST login", () => {
  it("Successfully Login", done => {
    request(app)
      .post("/auth/login")
      .send({
        username: "binar_std1",
        password: "binar123"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(200);
          expect(res.body.success).toBe(true);
          expect(res.body.message).toBe("user successfully login");
          expect(res.body).toHaveProperty("token");
          done();
        }
      })
  });

  it("Wrong username", done => {
    request(app)
      .post("/auth/login")
      .send({
        username: "binar_std",
        password: "binar123"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("wrong username");
          done();
        }
      })
  });

  it("Wrong password", done => {
    request(app)
      .post("/auth/login")
      .send({
        username: "binar_std1",
        password: "binar"
      })
      .end((err, res) => {
        if (err) {
          done(err);
        } else {
          expect(res.status).toBe(401);
          expect(res.body.success).toBe(false);
          expect(res.body.message).toBe("wrong password");
          done();
        }
      })
  })
})