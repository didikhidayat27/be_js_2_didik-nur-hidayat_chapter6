'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGameBiodata extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGameBiodata.belongsTo(models.UserGame, { foreignKey: 'user_game_id', as: 'user' })
    }
  }
  UserGameBiodata.init({
    user_game_id: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: DataTypes.STRING,
    age: DataTypes.INTEGER,
    about_me: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'UserGameBiodata',
  });
  return UserGameBiodata;
};