'use strict';
const {
  Model
} = require('sequelize');
const crypto = require('crypto');
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      UserGame.hasOne(models.UserGameBiodata, { foreignKey: 'user_game_id', as: 'biodata' })
      UserGame.hasMany(models.UserGameHistory, { foreignKey: 'user_game_id', as: 'game_histories' })
    }
  }
  UserGame.init({
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        len: [8, 20],
        notEmpty: true
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {
    hooks: {
      beforeCreate: (user, option) => {
        user.id = crypto.randomUUID()
      }
    },
    sequelize,
    modelName: 'UserGame',
  });
  return UserGame;
};