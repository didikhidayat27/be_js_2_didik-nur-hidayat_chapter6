require('dotenv').config();
const { UserGame } = require('../models');
const bcrypt       = require('bcrypt');
const jwt          = require('jsonwebtoken');
const {
  JWT_SECRET_KEY,
  JWT_EXPIRE_TIME
} = process.env;

class AuthController {
  static async login(req, res, next) {
    try {
      const { username, password } = req.body;

      if (username == undefined || password == undefined) {
        throw {
          code: "ERR_BAD_REQUEST",
          message: "username atau password tidak boleh kosong"
        }
      }

      const data = await UserGame.findOne({
        where: {
          username
        }
      }).catch(error => {
        throw {
          code: "ERR_BAD_REQUEST",
          message: error.message
        }
      });

      if (data === null) {
        throw {
          code: "ERR_UNAUTHORIZED",
          message: "wrong username"
        }
      } else {
        const passed = bcrypt.compareSync(password, data.password);
        if (passed) {
          const token = jwt.sign({
            id: data.id,
            username: data.username
          }, JWT_SECRET_KEY, { expiresIn: JWT_EXPIRE_TIME });

          return res.json({
            success: true,
            message: "user successfully login",
            token
          });
        } else {
          throw {
            code: "ERR_UNAUTHORIZED",
            message: "wrong password"
          }
        }
      }
    } catch (error) {
      next(error)
    }
  }
}

module.exports = AuthController;