const { UserGame, UserGameBiodata, UserGameHistory } = require('../models');
const bcrypt = require('bcrypt');

class UserGameController {
  // #swagger.tags = ['Users']
  static async getAll(req, res) {
    await UserGame.findAll({
      attributes: ['id', 'username']
    })
    .then(data => {
      return res.json({
        success: true,
        message: "data successfully retrieved",
        data
      })
    })
  }

  static async getDetail(req, res, next) {
    const { id } = req.params;
    // #swagger.tags = ['Users']
    try {
      const data = await UserGame.findOne({
        where: { id },
        include: [
          {
            model: UserGameBiodata,
            attributes: ['name', 'age', 'about_me'],
            as: 'biodata'
          },
          {
            model: UserGameHistory,
            as: 'game_histories'
          }
        ]
      })

      if (!data) {
        throw { code: "ERR_NOT_FOUND", message: "data not found" }
      }
      
      return res.json({
        success: true,
        message: "data successfully retrieved",
        data
      })
    } catch (error) {
      next(error)
    }
  }

  static async addUser(req, res, next) {
    const { username, password, name } = req.body;
    // #swagger.tags = ['Users']
    try {
      await UserGame.create({
        username, password: bcrypt.hashSync(password, 10),
        biodata: {
          name
        }
      }, {
        include: {
          model: UserGameBiodata,
          as: 'biodata',
        }
      })
      .then(() => {
        return res.status(201).json({
          success: true,
          message: 'data successfully added'
        })
      })
    } catch (error) {
      next(error);
    }
  }

  static async addGameHistory(req, res, next) {
    const { id } = req.user;
    const { point, level } = req.body;
    // #swagger.tags = ['Users']
    try {
      const checkAVB = await UserGame.findOne({ where: { id } });
      if (!checkAVB) {
        throw { code: "ERR_NOT_FOUND", message: "data not found" }
      }

      await UserGameHistory.create({
        point, level, user_game_id: id
      })
      .then(() => {
        return res.status(201).json({
          success: true,
          message: "game history successfully added"
        })
      })
    } catch (error) {
      next(error);
    }
  }

  static async updateUser(req, res, next) {
    const { name, age, about_me } = req.body;
    const { id } = req.params;
    // #swagger.tags = ['Users']
    try {
      const checkAVB = await UserGame.findOne({ where: { id } });
      if (!checkAVB) {
        throw { code: "ERR_NOT_FOUND", message: "data not found" }
      }

      await UserGameBiodata.update({ name, age: +age, about_me }, {
        where: { user_game_id: id }
      }).then(() => {
        return res.json({
          success: true,
          message: "data successfully updated"
        })
      })
    } catch (error) {
      next(error)
    }
  }

  static async deleteUser(req, res, next) {
    const { id } = req.params;
    // #swagger.tags = ['Users']
    try {
      const checkAVB = await UserGame.findOne({ where: { id } });
      if (!checkAVB) {
        throw { code: "ERR_NOT_FOUND", message: "data not found" }
      }

      await UserGame.destroy({
        where: { id }
      }).then(() => {
        return res.json({
          success: false,
          message: "data successfully deleted"
        })
      })
    } catch (error) {
      next(error)
    }
  }
}

module.exports = UserGameController;