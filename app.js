require('dotenv').config()
const express = require('express');
const logger = require('morgan');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const usersRouter = require('./routes/users');
const authRouter  = require('./routes/auth');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use('/auth', authRouter);
app.use('/users', usersRouter);

app.use((err, req, res, next) => {
  if (err.name == "SequelizeValidationError") {
    return res.status(422).json({
      success: false,
      message: err.message
    });
  } else if (err.code == "ERR_NOT_FOUND") {
    return res.status(404).json({
      success: false,
      message: err.message
    });
  } else if (err.code == "ERR_UNAUTHORIZED") {
    return res.status(401).json({
      success: false,
      message: err.message
    });
  } else if (err.code == "ERR_BAD_REQUEST") {
    return res.status(400).json({
      success: false,
      message: err.message
    });
  } else if (err.code == "ERR_FORBIDDEN") {
    return res.status(400).json({
      success: false,
      message: err.message
    });
  } else {
    return res.status(500).json({
      success: false,
      message: err.message
    });
  }
})

module.exports = app;
