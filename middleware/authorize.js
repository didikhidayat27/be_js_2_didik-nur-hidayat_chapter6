require('dotenv').config();
const jwt = require('jsonwebtoken');
const {
  JWT_SECRET_KEY
} = process.env;

module.exports = async (req, res, next) => {
  // check token
  const isExistToken = req.get('X-Api-Token');
  if (isExistToken == undefined || isExistToken.length == 0) {
    return res.status(401).json({
      success: false,
      message: "Invalid Token"
    })
  }

  // verify token
  const [Bearer, token] = isExistToken.split(' ');
  if (Bearer == "Bearer") {
    jwt.verify(token, JWT_SECRET_KEY, (err, passed) => {
      if (err) {
        return res.status(401).json({
          success: false,
          message: err.message
        });
      }

      req.user = passed;
      next();
    })
  } else {
    return res.status(401).json({
      success: false,
      message: "Invalid Token"
    });
  }
}
