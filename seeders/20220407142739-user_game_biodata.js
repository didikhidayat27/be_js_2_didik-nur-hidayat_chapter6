'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('UserGameBiodata', [
      { user_game_id: '1403603b-1a25-487d-b33a-88c6ffc77b37', name: 'Binar Student', age: 21, about_me: 'lorem ipsum' }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGameBiodata')
  }
};
