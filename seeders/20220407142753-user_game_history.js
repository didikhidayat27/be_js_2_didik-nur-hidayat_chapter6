'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('UserGameHistories', [
      { user_game_id: '1403603b-1a25-487d-b33a-88c6ffc77b37', point: 10, level: 1 },
      { user_game_id: '1403603b-1a25-487d-b33a-88c6ffc77b37', point: 20, level: 1 },
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGameHistories', {}, { restartIdentity: true });
  }
};
