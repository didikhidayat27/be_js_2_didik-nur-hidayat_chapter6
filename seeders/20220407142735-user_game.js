'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('UserGames', [
      { id: '1403603b-1a25-487d-b33a-88c6ffc77b37', username: 'binar_std1', password: bcrypt.hashSync('binar123', 10) }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('UserGames')
  }
};
