const router = require('express').Router();

// Controller
const controller = require('../controller/UsersGameController');

// middleware
const authorize = require('../middleware/authorize');

// GET Route
router.get('/', authorize, controller.getAll);
router.get('/:id', authorize, controller.getDetail);

// POST Route
router.post('/', authorize, controller.addUser);
router.post('/:id/history', authorize, controller.addGameHistory);

// PUT Route
router.put('/:id', authorize, controller.updateUser);

// DELETE Route
router.delete('/:id', authorize, controller.deleteUser);

module.exports = router;