const router = require('express').Router();

// controller
const controller = require('../controller/AuthController');

router.post('/login', controller.login);

module.exports = router;